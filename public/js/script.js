let language = 0;

$(document).ready(function () {
  $(".item-portuguese").click(function () {
    showPageInPortuguese();
  });
  $(".item-english").click(function () {
    showPageInEnglish();
  });

  let location = window.location.hash;
  if (location === "#en") {
    showPageInEnglish();
  } else {
    showPageInPortuguese();
  }
});

function showPageInEnglish() {
  language = 0;
  $(".item-english").addClass("link-disabled");
  $(".item-portuguese").removeClass("link-disabled");
  updateTexts();
  updateLinks();
}

function showPageInPortuguese() {
  language = 1;
  $(".item-portuguese").addClass("link-disabled");
  $(".item-english").removeClass("link-disabled");
  updateTexts();
  updateLinks();
}

function updateLinks() {
  if (language === 0) { // english
    let src = "https://www.youtube.com/watch?v=cipzLjPKYgY"
    $("#video-intelli-filter").attr({ 'src': src });
  } else if (language === 1) { // portuguese
    let src = "https://www.youtube.com/watch?v=cipzLjPKYgY"
    $("#video-intelli-filter").attr({ 'src': src });
  }
}

function updateTexts() {
  // index texts
  $(".text-home").html(HOME[language]);
  $(".text-menu").html(MENU[language]);
  $(".text-about").html(ABOUT[language]);
  $(".text-portfolio").html(PORTFOLIO[language]);
  $(".text-graduation").html(GRADUATION[language]);
  $(".text-contact").html(CONTACT[language]);
  $(".text-short").html(SHORT[language]);
  $(".text-short-me").html(SHORT_ME[language]);
  $(".text-bio").html(BIO[language]);
  $(".text-bio-intro").html(BIO_INTRO[language]);
  $(".text-bio-skills").html(BIO_SKILLS[language]);
  $(".text-ceap-short-name").html(CEAP_SHORT_NAME[language]);
  $(".text-ceap-full-name").html(CEAP_FULL_NAME[language]);
  $(".text-ufabc-short-name").html(UFABC_SHORT_NAME[language]);
  $(".text-ufabc-full-name").html(UFABC_FULL_NAME[language]);
  $(".text-degree-bcc").html(DEGREE_BCC[language]);
  $(".text-degree-bct").html(DEGREE_BCT[language]);
  $(".text-degree-electronic-technician").html(DEGREE_ELECTRONIC_TECHNICIAN[language]);
  $(".text-experience").html(EXPERIENCE[language]);
  $(".text-experience-totvs").html(EXPERIENCE_TOTVS[language]);
  $(".text-experience-totvs-time").html(EXPERIENCE_TOTVS_TIME[language]);
  $(".text-experience-tecsidel").html(EXPERIENCE_TECSIDEL[language]);
  $(".text-experience-tecsidel-time").html(EXPERIENCE_TECSIDEL_TIME[language]);
  $(".text-experience-zeppelin").html(EXPERIENCE_ZEPPELIN[language]);
  $(".text-experience-zeppelin-time").html(EXPERIENCE_ZEPPELIN_TIME[language]);
  $(".text-experience-lsitec").html(EXPERIENCE_LSITEC[language]);
  $(".text-experience-lsitec-time").html(EXPERIENCE_LSITEC_TIME[language]);
  $(".text-experience-gft").html(EXPERIENCE_GFT[language]);
  $(".text-experience-gft-time").html(EXPERIENCE_GFT_TIME[language]);
  $(".text-name").html(NAME[language]);
  $(".text-zeppelin-fullname").html(ZEPPELIN_FULLNAME[language]);
  $(".text-tecsidel-fullname").html(TECSIDEL_FULLNAME[language]);
  $(".text-particular-project").html(PARTICULAR_PROJECT[language]);
  // modal texts
  $(".text-close").html(CLOSE[language]);
  $(".text-company").html(COMPANY[language]);
  $(".text-video").html(VIDEO[language]);
  $(".text-image").html(IMAGE[language]);
  $(".text-website").html(WEBSITE[language]);
  $(".text-skills-needed").html(SKILLS_NEEDED[language]);
  $(".text-versioning").html(VERSIONING[language]);
  $(".text-team").html(TEAM[language]);
  $(".text-me").html(ME[language]);
  $(".text-intern").html(INTERN[language]);
  $(".text-developer").html(DEVELOPER[language]);
  // footer texts
  $(".text-mail").html(MAIL[language]);
  $(".text-web").html(WEB[language]);
  $(".text-social-medias").html(SOCIAL_MEDIAS[language]);
  // intelli-filter texts modal
  $(".text-intelli-filter").html(INTELLI_FILTER[language]);
  $(".text-intelli-filter-short").html(INTELLI_FILTER_SHORT[language]);
  $(".text-intelli-filter-versioning-p").html(INTELLI_FILTER_VERSIONING_P[language]);
  $(".text-to-board").html(TO_BOARD[language]);
  $(".text-skills-to-board").html(SKILLS_TO_BOARD[language]);
  $(".text-to-sequencer-firmware").html(TO_SEQUENCER_FIRMWARE[language]);
  $(".text-skills-to-sequencer-firmware").html(SKILLS_TO_SEQUENCER_FIRMWARE[language]);
  $(".text-to-wifi-firmware").html(TO_WIFI_FIRMWARE[language]);
  $(".text-skills-to-wifi-firmware").html(SKILLS_TO_WIFI_FIRMWARE[language]);
  $(".text-to-android-app").html(TO_ANDROID_APP[language]);
  $(".text-skills-to-android-app").html(SKILLS_TO_ANDROID_APP[language]);
  $(".text-to-web-server").html(TO_WEB_SERVER[language]);
  $(".text-skills-to-web-server").html(SKILLS_TO_WEB_SERVER[language]);
  $(".text-intelli-filter-tasks-me").html(INTELLI_FILTER_TASKS_ME[language]);
  $(".text-intelli-filter-tasks-intern-1").html(INTELLI_FILTER_TASKS_INTERN_1[language]);
  $(".text-intelli-filter-tasks-intern-2").html(INTELLI_FILTER_TASKS_INTERN_2[language]);
  // zeppdocs texts modal
  $(".text-zeppdocs").html(ZEPPDOCS[language]);
  $(".text-zeppdocs-short").html(ZEPPDOCS_SHORT[language]);
  $(".text-zeppdocs-versioning-p").html(ZEPPDOCS_VERSIONING_P[language]);
  $(".text-skills-to-zeppdocs-web-server").html(SKILLS_TO_ZEPPDOCS_WEB_SERVER[language]);
  $(".text-zeppdocs-tasks-me").html(ZEPPDOCS_TASKS_ME[language]);
  $(".text-zeppdocs-tasks-intern-1").html(ZEPPDOCS_INTERN_1[language]);
  // zeppshop texts modal
  $(".text-zeppshop").html(ZEPPSHOP[language]);
  $(".text-zeppshop-short").html(ZEPPSHOP_SHORT[language]);
  $(".text-zeppshop-versioning-p").html(ZEPPSHOP_VERSIONING_P[language]);
  $(".text-skills-to-zeppshop-web-server").html(SKILLS_TO_ZEPPSHOP_WEB_SERVER[language]);
  $(".text-zeppshop-tasks-me").html(ZEPPSHOP_TASKS_ME[language]);
  // gpie texts modal
  $(".text-gpie").html(GPIE[language]);
  $(".text-gpie-short").html(GPIE_SHORT[language]);
  $(".text-gpie-versioning-p").html(GPIE_VERSIONING_P[language]);
  $(".text-skills-to-gpie-web-server").html(SKILLS_TO_GPIE_WEB_SERVER[language]);
  $(".text-gpie-tasks-me").html(GPIE_TASKS_ME[language]);
  // titogi texts modal
  $(".text-titogi").html(TITOGI[language]);
  $(".text-titogi-short").html(TITOGI_SHORT[language]);
  $(".text-titogi-versioning-p").html(TITOGI_VERSIONING_P[language]);
  $(".text-skills-to-titogi-web-server").html(SKILLS_TO_TITOGI_WEB_SERVER[language]);
  $(".text-titogi-tasks-me").html(TITOGI_TASKS_ME[language]);
  $(".text-titogi-tasks-developer-1").html(TITOGI_TASKS_DEVELOPER_1[language]);
  $(".text-titogi-tasks-developer-2").html(TITOGI_TASKS_DEVELOPER_2[language]);
  // igr-token texts modal
  $(".text-igr-token").html(IGRTOKEN[language]);
  $(".text-igr-token-short").html(IGRTOKEN_SHORT[language]);
  $(".text-igr-token-versioning-p").html(IGRTOKEN_VERSIONING_P[language]);
  $(".text-skills-to-igr-token-android-app").html(SKILLS_TO_IGRTOKEN_ANDROID_APP[language]);
  $(".text-igr-token-tasks-me").html(IGRTOKEN_TASKS_ME[language]);
  // sgp texts modal
  $(".text-sgp").html(SGP[language]);
  $(".text-sgp-short").html(SGP_SHORT[language]);
  $(".text-sgp-versioning").html(SGP_VERSIONING[language]);
  $(".text-skills-to-sgp-android-app").html(SKILLS_TO_SGP_ANDROID_APP[language]);
  $(".text-sgp-tasks-me").html(SGP_TASKS_ME[language]);
  // nvmsim texts modal
  $(".text-nvmsim").html(NVMSIM[language]);
  $(".text-nvmsim-short").html(NVMSIM_SHORT[language]);
  $(".text-skills-to-nvmsim").html(SKILLS_TO_NVMSIM[language]);
  $(".text-nvmsim-versioning").html(NVMSIM_VERSIONING[language]);
  $(".text-nvmsim-tasks-me").html(NVMSIM_TASKS_ME[language]);
}
